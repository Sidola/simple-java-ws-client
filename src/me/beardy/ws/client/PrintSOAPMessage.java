package me.beardy.ws.client;

import java.io.StringWriter;

import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Utility class to print a SOAPMessage object as XML.
 * 
 * Source: http://stackoverflow.com/a/20091239/1891491
 */
public class PrintSOAPMessage {

	public static void print(SOAPMessage message) {
		
        final StringWriter sw = new StringWriter();

        try {
            TransformerFactory.newInstance().newTransformer().transform(
                new DOMSource(message.getSOAPPart()),
                new StreamResult(sw));
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }

        // Now you have the XML as a String:
        System.out.println(sw.toString());
		
	}
	
}
