package me.beardy.ws.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;

/**
 * This client creates it's own SOAP message, and as such it doesn't have to
 * first generate a .jar from the WSDL.
 * 
 * @author Sid
 */
public class HelloWorldSOAPClient {

	private static final Logger LOG = Logger
			.getLogger(HelloWorldSOAPClient.class.getName());

	private static final String ENDPOINT_URL = "http://localhost:9999/hello?WSDL";
	private static final String XMLNS = "http://hello.ws.beardy.me/";

	private static SOAPFactory soapFactory;
	private static MessageFactory messageFactory;

	/**
	 * Main class, instantiates some factories and calls the server.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			soapFactory = SOAPFactory.newInstance();
			messageFactory = MessageFactory.newInstance();
		} catch (SOAPException e1) {
			e1.printStackTrace();
		}

		callSay("Hello, this is the SAAJ client!");
		callYell("Hi, it's me again!");

	}

	/**
	 * This method constructs a valid SOAP message for the 'say' method.
	 * 
	 * @param message
	 *            Message to send.
	 */
	private static void callSay(String message) {
		try {
			SOAPMessage soapMessage = messageFactory.createMessage();
			SOAPBody soapBody = soapMessage.getSOAPBody();

			// <m:say xmlns:m="http://hello.ws.beardy.me/">
			Name bodyName = soapFactory.createName("say", "m", XMLNS);
			SOAPBodyElement soapBodyElement = soapBody.addBodyElement(bodyName);

			// <m:say xmlns:m="http://hello.ws.beardy.me/">
			// 		<str>Testing, testing, one two, one two.</str>
			// </m:say>
			Name name = soapFactory.createName("str");
			SOAPElement param = soapBodyElement.addChildElement(name);
			param.addTextNode(message);

			// Send the message
			sendSOAPMessage(soapMessage);
		} catch (SOAPException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method constructs a valid SOAP message for the 'yell' method.
	 * 
	 * @param message
	 *            Message to send.
	 */
	private static void callYell(String message) {
		try {
			SOAPMessage soapMessage = messageFactory.createMessage();
			SOAPBody soapBody = soapMessage.getSOAPBody();

			// <m:yell xmlns:m="http://hello.ws.beardy.me/">
			Name bodyName = soapFactory.createName("yell", "m", XMLNS);
			SOAPBodyElement soapBodyElement = soapBody.addBodyElement(bodyName);

			// <m:yell xmlns:m="http://hello.ws.beardy.me/">
			// <str>Testing, testing, one two, one two.</str>
			// </m:say>
			Name name = soapFactory.createName("str");
			SOAPElement param = soapBodyElement.addChildElement(name);
			param.addTextNode(message);

			// Send the message
			sendSOAPMessage(soapMessage);
		} catch (SOAPException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method sends the actual message to the server.
	 * 
	 * @param soapMessage
	 *            SOAPMessage instance to send to the server.
	 */
	private static void sendSOAPMessage(SOAPMessage soapMessage) {

		// Uncomment the line below to print out the XML for the SOAP
		// message.
		// PrintSOAPMessage.print(soapMessage);

		try {

			// Create SOAP connection
			URL endpointURL = new URL(ENDPOINT_URL);
			SOAPConnectionFactory scf = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = scf.createConnection();

			// Send a SOAP message using the connection.
			SOAPMessage response = soapConnection
					.call(soapMessage, endpointURL);
			soapConnection.close();

			// Process the response message
			SOAPBody respBody = response.getSOAPBody();
			Iterator it = respBody.getChildElements();

			SOAPBodyElement respBodyElement = (SOAPBodyElement) it.next();
			LOG.info("-- Response = " + respBodyElement.getNodeName());

			it = respBodyElement.getChildElements();
			SOAPElement ret = (SOAPElement) it.next();
			LOG.info("-- " + ret.getNodeName() + " = " + ret.getTextContent());

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SOAPException e) {
			e.printStackTrace();
		}
	}
}